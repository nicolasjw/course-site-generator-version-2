package csb.controller;

import static csb.CSB_PropertyType.REMOVE_ITEM_MESSAGE;
import csb.data.Assignment;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.ScheduleItem;
import csb.data.Lecture;
import csb.gui.CSB_GUI;
import csb.gui.MessageDialog;
import csb.gui.ScheduleItemDialog;
import csb.gui.LectureItemDialog; // IMPORT LECTURE DIALOG
import csb.gui.AssignmentItemDialog; // IMPORT ASSIGNMENT DOALOG
import csb.gui.YesNoCancelDialog;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author McKillaGorilla
 */
public class ScheduleEditController {
    ScheduleItemDialog sid;
    LectureItemDialog lid; //create instance of lecture dialog
    AssignmentItemDialog aid; //create instance of lecture dialog
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    public ScheduleEditController(Stage initPrimaryStage, Course course, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        sid = new ScheduleItemDialog(initPrimaryStage, course, initMessageDialog);
        lid = new LectureItemDialog(initPrimaryStage, course, initMessageDialog);
        aid = new AssignmentItemDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    // THESE ARE FOR SCHEDULE ITEMS
    
    public void handleAddScheduleItemRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        sid.showAddScheduleItemDialog(course.getStartingMonday());
        
        // DID THE USER CONFIRM?
        if (sid.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            ScheduleItem si = sid.getScheduleItem();
            
            // AND ADD IT AS A ROW TO THE TABLE
            course.addScheduleItem(si);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleEditScheduleItemRequest(CSB_GUI gui, ScheduleItem itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        sid.showEditScheduleItemDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (sid.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            ScheduleItem si = sid.getScheduleItem();
            itemToEdit.setDescription(si.getDescription());
            itemToEdit.setDate(si.getDate());
            itemToEdit.setLink(si.getLink());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    
    public void handleRemoveScheduleItemRequest(CSB_GUI gui, ScheduleItem itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeScheduleItem(itemToRemove);
        }
    }
    
    //HAD TO ADD THESE METHODS IN ORDER TO HANDLE LECTUREITEM REQUESTS
    
    public void handleAddLectureItemRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        lid.showAddLectureItemDialog();
        
        // DID THIS USER CONFIRM?
        if (lid.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            Lecture lecture = lid.getLectureItem();
            
            // AND ADD IT AS ROW TO THE TABLE
            course.addLecture(lecture);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleEditLectureItemRequest(CSB_GUI gui, Lecture itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        lid.showEditLectureItemDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (lid.wasCompleteSelected()) {
            // UPDATE THE LECTURE ITEM
            Lecture lecture = lid.getLectureItem();
            itemToEdit.setTopic(lecture.getTopic());
            itemToEdit.setSessions(lecture.getSessions());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleRemoveLectureItemRequest(CSB_GUI gui, Lecture itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();
        
        // IF THE USER SAID YES, THEN SAVVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) {
            gui.getDataManager().getCourse().removeLecture(itemToRemove);
        }
    }
    
    public void handleMoveLectureUp(CSB_GUI gui, int selectedIndex) {
        ObservableList<Lecture> list = gui.getDataManager().getCourse().getLectures();
        if(selectedIndex == 0) // SELECTION IS FIRST ITEM, DO NOTHING
            return;
        else{ // MOVE UP
            Lecture temp = list.get(selectedIndex);
            list.set(selectedIndex, list.get(selectedIndex-1));
            list.set(selectedIndex-1, temp);
        }
    }
    public void handleMoveLectureDown(CSB_GUI gui, int selectedIndex) {
        ObservableList<Lecture> list = gui.getDataManager().getCourse().getLectures();
        if(selectedIndex == list.size()-1) // SELECTION IS LAST ITEM, DO NOTHING
            return;
        else{ // MOVE DOWN
            Lecture temp = list.get(selectedIndex);
            list.set(selectedIndex, list.get(selectedIndex+1));
            list.set(selectedIndex+1, temp);
        }
    }    
    
    // THESE ARE FOR THE ASSIGNMENT ITEMS
    public void handleAddAssignmentItemRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        aid.showAddAssignmentItemDialog(course.getStartingMonday());
        
        // DID THE USER CONFIRM
        if (aid.wasCompleteSelected()) {
            // GET THE ASSIGNMENT ITEM
            Assignment assignment = aid.getAssignmentItem();
            
            // AND ADD IT AS A ROW TO THE TABLE
            course.addAssignment(assignment);
        }
        else{
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleEditAssignmentItemRequest(CSB_GUI gui, Assignment itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        aid.showEditAssignmentItemDialog(itemToEdit);
        
        // DID THE USER CONFIRM
        if (aid.wasCompleteSelected()) {
            // UPDATE THE ASSIGNMENT ITEM
            Assignment assignment = aid.getAssignmentItem();
            itemToEdit.setName(assignment.getName());
            itemToEdit.setDate(assignment.getDate());
            itemToEdit.setTopics(assignment.getTopics());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleRemoveAssignmentItemRequest(CSB_GUI gui, Assignment itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();
        
        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) {
            gui.getDataManager().getCourse().removeAssignment(itemToRemove);
        }
    }
}