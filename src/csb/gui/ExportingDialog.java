package csb.gui;


import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author McKillaGorilla
 */
public class ExportingDialog extends Stage {
    // THIS IS THE OBJECT DATA BEHIND THIS UI
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label exportingLabel; 
    ProgressBar progressBar;
    ProgressIndicator progressIndicator;
    
    
    // CONSTANTS FOR OUR UI

    /**
     * Initializes this dialog so that it can be used for either adding
     * new schedule items or editing existing ones.
     * 
     * @param primaryStage The owner of this modal dialog.
     */
    public ExportingDialog(Stage primaryStage) {       
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // INIT ELEMENTS
        exportingLabel = new Label();
        exportingLabel.setFont(new Font("Serif", 32));
        progressBar = new ProgressBar(0);
        progressIndicator = new ProgressIndicator(0);
        
        // PLACE ELEMENTS
        gridPane.add(exportingLabel, 0, 0, 1, 1);
        gridPane.add(progressBar, 0, 1, 1, 1);
        gridPane.add(progressIndicator, 1, 1, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        
        progressBar.setPrefWidth(500);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
        
        // SET DIALOG SIZE
        this.setHeight(300);
        this.setWidth(700);
    }
    
    public void updateExporting(String name, double percent) { 
        exportingLabel.setText("Exporting " + name + " Completed");
        progressBar.setProgress(percent);
        progressIndicator.setProgress(percent);
    }
    
}